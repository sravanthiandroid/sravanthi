package com.sravanthi.app.weatherapp.ui.search.result

import androidx.databinding.ObservableField
import com.sravanthi.app.weatherapp.core.BaseViewModel
import com.sravanthi.app.weatherapp.db.entity.CitiesForSearchEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 07-08-2021
 */

@HiltViewModel
class SearchResultItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<CitiesForSearchEntity>()
}

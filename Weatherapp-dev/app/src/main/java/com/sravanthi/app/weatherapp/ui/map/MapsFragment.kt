package com.sravanthi.app.weatherapp.ui.map

import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.sravanthi.app.weatherapp.R
import com.sravanthi.app.weatherapp.core.BaseFragment
import com.sravanthi.app.weatherapp.core.Constants
import com.sravanthi.app.weatherapp.databinding.FragmentMapsBinding
import com.sravanthi.app.weatherapp.db.entity.CoordEntity
import com.sravanthi.app.weatherapp.domain.usecase.CurrentWeatherUseCase
import com.sravanthi.app.weatherapp.domain.usecase.ForecastUseCase
import com.sravanthi.app.weatherapp.ui.main.MainActivity
import com.sravanthi.app.weatherapp.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MapsFragment :BaseFragment<MapsViewModel, FragmentMapsBinding>(
R.layout.fragment_maps,
    MapsViewModel::class.java,
) ,GoogleMap.OnMarkerDragListener {
    private lateinit var googleMap: GoogleMap
    private lateinit var startMarker: Marker
    private lateinit var finishMarker: Marker
    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        this.googleMap = googleMap
        val lat = 17.3850
        val lng = 78.4867
        val sydney = LatLng(lat, lng)
        startMarker = googleMap.addMarker(
            MarkerOptions().position(sydney).title("Marker in Sydney").draggable(true)
        )
        val cameraPosition = CameraPosition.Builder()
            .target(LatLng(lat, lng))
            .zoom(6f)
            .build()
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        googleMap.setOnMarkerDragListener(this)
        googleMap.setOnMapClickListener {
            googleMap.clear()
            googleMap.addMarker(MarkerOptions().position(it))
            saveCoordsToSharedPref(it.latitude, it.longitude, "")
            findNavController().navigate(
                R.id.action_mapFragment_to_dashboardFragment
            )
        }

    }


    private fun setFinishLocation(lat: Double, lng: Double, addr: String) {
        var address = "Destination address"
        if (addr.isEmpty()) {
            val gcd = Geocoder(requireActivity(), Locale.getDefault())
            val addresses: List<Address>
            try {
                addresses = gcd.getFromLocation(lat, lng, 1)
                if (addresses.isNotEmpty()) {
                    address = addresses[0].getAddressLine(0)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            address = addr
        }
        googleMap.clear();
        finishMarker = googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(lat, lng))
                .title("Finish Location")
                .snippet("Near $address")
                .draggable(true)
        )
        val cameraPosition = CameraPosition.Builder()
            .target(LatLng(lat, lng))
            .zoom(7f)
            .build()
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        saveCoordsToSharedPref(lat, lng, address)
        findNavController().navigate(
            R.id.action_mapFragment_to_dashboardFragment
        )

    }

    private fun saveCoordsToSharedPref(lat: Double, lng: Double, address: String) {

        binding.viewModel?.pref!!.edit().putString(Constants.Coords.LAT, lat.toString()).apply()
        binding.viewModel?.pref!!.edit().putString(Constants.Coords.LON,lng.toString()).apply()
        binding.viewModel?.pref!!.edit().putString(Constants.ADDRESS,"").apply()

    }



    private fun setStartLocation(lat: Double, lng: Double, addr: String) {
        var address = "Current address"
        if (addr.isEmpty()) {
            val gcd = Geocoder(requireActivity(), Locale.getDefault())
            val addresses: List<Address>
            try {
                addresses = gcd.getFromLocation(lat, lng, 1)
                if (addresses.isNotEmpty()) {
                    address = addresses[0].getAddressLine(0)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            address = addr
        }
        googleMap.clear();
        startMarker = googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(lat, lng))
                .title("Start Location")
                .snippet("Near $address")
                .draggable(true)
        )
        val cameraPosition = CameraPosition.Builder()
            .target(LatLng(lat, lng))
            .zoom(11f)
            .build()
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        saveCoordsToSharedPref(lat, lng, address)
        findNavController().navigate(
            R.id.action_mapFragment_to_dashboardFragment
        )

    }
    override fun init() {
        super.init()
        val mapFragment = childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }


    override fun onMarkerDragStart(p0: Marker) {
    }

    override fun onMarkerDrag(p0: Marker) {
    }

    override fun onMarkerDragEnd(p0: Marker) {
        if (p0 == startMarker) {
            setStartLocation(p0.position.latitude, p0.position.longitude, "")
        } else if (p0 == finishMarker) {
            setFinishLocation(p0.position.latitude, p0.position.longitude, "")
        }
    }

}
package com.sravanthi.app.weatherapp.domain.datasource.forecast

import com.sravanthi.app.weatherapp.domain.WeatherAppAPI
import com.sravanthi.app.weatherapp.domain.model.ForecastResponse
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class ForecastRemoteDataSource @Inject constructor(private val api: WeatherAppAPI) {

    fun getForecastByGeoCords(lat: Double, lon: Double, units: String): Single<ForecastResponse> = api.getForecastByGeoCords(
        lat,
        lon,
        units
    )
}

package com.sravanthi.app.weatherapp.utils

/**
 * Created by sravanthi madhapuram on 08-08-2021
 */

interface Mapper<R, D> {
    fun mapFrom(type: R): D
}

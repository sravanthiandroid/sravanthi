package com.sravanthi.app.weatherapp.utils.domain

/**
 * Created by sravanthi madhapuram on 08-08-2021
 */

// references :
// https://developer.android.com/jetpack/docs/guide#addendum

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}

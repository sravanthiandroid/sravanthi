package com.sravanthi.app.weatherapp.ui.dashboard.forecast

import androidx.databinding.ObservableField
import com.sravanthi.app.weatherapp.core.BaseViewModel
import com.sravanthi.app.weatherapp.domain.model.ListItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

@HiltViewModel
class ForecastItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<ListItem>()
}

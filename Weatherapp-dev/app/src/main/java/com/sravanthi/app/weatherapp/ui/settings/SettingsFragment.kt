package com.sravanthi.app.weatherapp.ui.settings

import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.sravanthi.app.weatherapp.R
import com.sravanthi.app.weatherapp.core.BaseFragment
import com.sravanthi.app.weatherapp.core.Constants
import com.sravanthi.app.weatherapp.databinding.FragmentMapsBinding
import com.sravanthi.app.weatherapp.databinding.FragmentSettingsBinding
import com.sravanthi.app.weatherapp.db.entity.CoordEntity
import com.sravanthi.app.weatherapp.domain.usecase.CurrentWeatherUseCase
import com.sravanthi.app.weatherapp.domain.usecase.ForecastUseCase
import com.sravanthi.app.weatherapp.ui.main.MainActivity
import com.sravanthi.app.weatherapp.utils.Utils
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashSet

@AndroidEntryPoint
class SettingsFragment :BaseFragment<SettingsViewModel, FragmentSettingsBinding>(
R.layout.fragment_settings,
    SettingsViewModel::class.java,
), View.OnClickListener {

    override fun init() {
        super.init()
      binding.clearBtn.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
//        binding.viewModel?.pref!!.edit().putString(Constants.Coords.LAT, "").apply()
//        binding.viewModel?.pref!!.edit().putString(Constants.Coords.LON,"").apply()
//        binding.viewModel?.pref!!.edit().putString(Constants.ADDRESS,"").apply()
//
binding.viewModel?.pref?.edit()!!.putStringSet(Constants.LATLANG, HashSet<String>())
            .apply()
        findNavController().navigate(
            R.id.action_settingsFragment_to_dashboardFragment
        )
    }


}
package com.sravanthi.app.weatherapp.domain.datasource.currentWeather

import com.sravanthi.app.weatherapp.domain.WeatherAppAPI
import com.sravanthi.app.weatherapp.domain.model.CurrentWeatherResponse
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class CurrentWeatherRemoteDataSource @Inject constructor(private val api: WeatherAppAPI) {

    fun getCurrentWeatherByGeoCords(lat: Double, lon: Double, units: String): Single<CurrentWeatherResponse> = api.getCurrentByGeoCords(
        lat,
        lon,
        units
    )
}

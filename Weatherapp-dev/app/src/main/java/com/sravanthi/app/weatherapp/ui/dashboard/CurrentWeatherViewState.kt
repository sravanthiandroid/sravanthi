package com.sravanthi.app.weatherapp.ui.dashboard

import com.sravanthi.app.weatherapp.core.BaseViewState
import com.sravanthi.app.weatherapp.db.entity.CurrentWeatherEntity
import com.sravanthi.app.weatherapp.utils.domain.Status

/**
 * Created by Sravanthi Madhapuram on 08-08-2021
 */

class CurrentWeatherViewState(
    val status: Status,
    val error: String? = null,
    val data: CurrentWeatherEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}

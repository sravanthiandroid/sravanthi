package com.sravanthi.app.weatherapp.ui.weather_detail.weatherHourOfDay

import androidx.databinding.ObservableField
import com.sravanthi.app.weatherapp.core.BaseViewModel
import com.sravanthi.app.weatherapp.domain.model.ListItem
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 08-08-2021
 */

class WeatherHourOfDayItemViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<ListItem>()
}

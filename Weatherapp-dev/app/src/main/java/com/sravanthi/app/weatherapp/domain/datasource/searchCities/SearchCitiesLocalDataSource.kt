package com.sravanthi.app.weatherapp.domain.datasource.searchCities

import androidx.lifecycle.LiveData
import com.sravanthi.app.weatherapp.db.dao.CitiesForSearchDao
import com.sravanthi.app.weatherapp.db.entity.CitiesForSearchEntity
import com.sravanthi.app.weatherapp.domain.model.SearchResponse
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class SearchCitiesLocalDataSource @Inject constructor(
    private val citiesForSearchDao: CitiesForSearchDao
) {

    fun getCityByName(cityName: String?): LiveData<List<CitiesForSearchEntity>> = citiesForSearchDao.getCityByName(
        cityName
    )

    fun insertCities(response: SearchResponse) {
        response.hits
            ?.map { CitiesForSearchEntity(it) }
            ?.let { citiesForSearchDao.insertCities(it) }
    }
}

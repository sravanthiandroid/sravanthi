package com.sravanthi.app.weatherapp.domain.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

 class LocationCoord(

    val lon: Double?,

    val lat: Double?,
    val address: String?

 )

package com.sravanthi.app.weatherapp.domain.datasource.forecast

import com.sravanthi.app.weatherapp.db.dao.ForecastDao
import com.sravanthi.app.weatherapp.db.entity.ForecastEntity
import com.sravanthi.app.weatherapp.domain.model.ForecastResponse
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class ForecastLocalDataSource @Inject constructor(private val forecastDao: ForecastDao) {

    fun getForecast() = forecastDao.getForecast()

    fun insertForecast(forecast: ForecastResponse) = forecastDao.deleteAndInsert(
        ForecastEntity(forecast)
    )
}

package com.sravanthi.app.weatherapp.core

import androidx.lifecycle.ViewModel

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

open class BaseViewModel : ViewModel()

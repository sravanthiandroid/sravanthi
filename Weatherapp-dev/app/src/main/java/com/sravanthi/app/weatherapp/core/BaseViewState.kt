package com.sravanthi.app.weatherapp.core

import com.sravanthi.app.weatherapp.utils.domain.Status

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

open class BaseViewState(val baseStatus: Status, val baseError: String?) {
    fun isLoading() = baseStatus == Status.LOADING
    fun getErrorMessage() = baseError
    fun shouldShowErrorMessage() = baseError != null
}

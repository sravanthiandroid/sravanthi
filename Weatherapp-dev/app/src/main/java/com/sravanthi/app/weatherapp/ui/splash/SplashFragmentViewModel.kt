package com.sravanthi.app.weatherapp.ui.splash

import android.content.SharedPreferences
import com.sravanthi.app.weatherapp.core.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 07-08-2021
 */

@HiltViewModel
class SplashFragmentViewModel @Inject constructor(
    var sharedPreferences: SharedPreferences
) : BaseViewModel() {
    var navigateDashboard = true
}

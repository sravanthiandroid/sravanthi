package com.sravanthi.app.weatherapp.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sravanthi.app.weatherapp.R
import com.sravanthi.app.weatherapp.databinding.DashboardCityItemsBinding

class DashboardCityListAdapter(

    private val listener: CityItemClickListener
) :
    RecyclerView.Adapter<DashboardCityListAdapter.CityViewHolder>() {

    private var cityList = ArrayList<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val groupItemBinding: DashboardCityItemsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.dashboard_city_items, parent, false
        )
        return CityViewHolder(groupItemBinding)
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        holder.groupItemBinding.cityName.text=cityList.get(position).split("@@")[2]
        holder.itemView.setOnClickListener {
            listener.onItemClick(
                cityList.get(position),
                position
            )
        }
    }

    override fun getItemCount(): Int {
        return cityList.size
    }

    fun setData(citysList: ArrayList<String>) {
        this.cityList = citysList

        notifyDataSetChanged()
    }


    class CityViewHolder(val groupItemBinding: DashboardCityItemsBinding) :
        RecyclerView.ViewHolder(groupItemBinding.root)

    interface CityItemClickListener {
        fun onItemClick(item: String?, position: Int)
    }
}

package com.sravanthi.app.weatherapp.ui.dashboard

import android.transition.TransitionInflater
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sravanthi.app.weatherapp.R
import com.sravanthi.app.weatherapp.core.BaseFragment
import com.sravanthi.app.weatherapp.core.Constants
import com.sravanthi.app.weatherapp.databinding.FragmentDashboardBinding
import com.sravanthi.app.weatherapp.domain.model.ListItem
import com.sravanthi.app.weatherapp.domain.usecase.CurrentWeatherUseCase
import com.sravanthi.app.weatherapp.domain.usecase.ForecastUseCase
import com.sravanthi.app.weatherapp.ui.dashboard.forecast.ForecastAdapter
import com.sravanthi.app.weatherapp.ui.main.MainActivity
import com.sravanthi.app.weatherapp.utils.extensions.isNetworkAvailable
import com.sravanthi.app.weatherapp.utils.extensions.observeWith
import dagger.hilt.android.AndroidEntryPoint
import java.util.HashSet

@AndroidEntryPoint
class DashboardFragment : BaseFragment<DashboardFragmentViewModel, FragmentDashboardBinding>(
    R.layout.fragment_dashboard,
    DashboardFragmentViewModel::class.java,
), DashboardCityListAdapter.CityItemClickListener {

    private var dashboardCommunityListAdapter: DashboardCityListAdapter? = null

    override fun init() {
        super.init()
        initForecastAdapter()
        initCityAdapter()

        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(
            android.R.transition.move
        )

        val lat: String? =
            binding.viewModel?.sharedPreferences?.getString(Constants.Coords.LAT, "17.3850")
        val lon: String? =
            binding.viewModel?.sharedPreferences?.getString(Constants.Coords.LON, "78.4867")
        var address: String? =
            binding.viewModel?.sharedPreferences?.getString(Constants.ADDRESS, "")
        if (lat?.isNotEmpty() == true && lon?.isNotEmpty() == true) {
            binding.viewModel?.setCurrentWeatherParams(
                CurrentWeatherUseCase.CurrentWeatherParams(
                    lat,
                    lon,
                    isNetworkAvailable(requireContext()),
                    Constants.Coords.METRIC
                )
            )
            binding.viewModel?.setForecastParams(
                ForecastUseCase.ForecastParams(
                    lat,
                    lon,
                    isNetworkAvailable(requireContext()),
                    Constants.Coords.METRIC
                )
            )
        }

        binding.viewModel?.getForecastViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                viewState = it
                it.data?.list?.let { forecasts -> initForecast(forecasts) }

                if (address?.isEmpty() == true) {
                    var city = it.data?.city
                    if (setDecimal(lat?.toDouble() )== setDecimal(city?.cityCoord?.lat )&& setDecimal(lon?.toDouble()) == setDecimal(city?.cityCoord?.lon)) {
                        address = city?.getCityAndCountry().toString()
                        saveCoordsToSharedPref(
                            lat!!, lon!!,
                            address!!
                        )
                    }
                }


                (activity as MainActivity).viewModel.toolbarTitle.set(
                    it.data?.city?.getCityAndCountry()
                )
            }
        }

        binding.viewModel?.getCurrentWeatherViewState()?.observeWith(
            viewLifecycleOwner
        ) {
            with(binding) {
                containerForecast.viewState = it
            }
        }
    }

    private fun setDecimal(toDouble: Double?): Double {
        return if(toDouble!=null)String.format("%.4f", toDouble).toDouble() else 0.0

    }

    private fun initCityAdapter() {


        binding.cityList.layoutManager = LinearLayoutManager(
            activity,
            RecyclerView.HORIZONTAL,
            false
        )
        dashboardCommunityListAdapter = DashboardCityListAdapter(
            this
        )
        binding.cityList.adapter = dashboardCommunityListAdapter
        setAdapter()
    }

    private fun initForecastAdapter() {
        val adapter = ForecastAdapter { item, cardView, forecastIcon, dayOfWeek, temp, tempMaxMin ->
            val action = DashboardFragmentDirections.actionDashboardFragmentToWeatherDetailFragment(
                item
            )
            findNavController()
                .navigate(
                    action,
                    FragmentNavigator.Extras.Builder()
                        .addSharedElements(
                            mapOf(
                                cardView to cardView.transitionName,
                                forecastIcon to forecastIcon.transitionName,
                                dayOfWeek to dayOfWeek.transitionName,
                                temp to temp.transitionName,
                                tempMaxMin to tempMaxMin.transitionName
                            )
                        )
                        .build()
                )
        }

        binding.recyclerForecast.adapter = adapter
        binding.recyclerForecast.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        postponeEnterTransition()
        binding.recyclerForecast.viewTreeObserver
            .addOnPreDrawListener {
                startPostponedEnterTransition()
                true
            }
    }

    private fun initForecast(list: List<ListItem>) {
        (binding.recyclerForecast.adapter as ForecastAdapter).submitList(list)
    }

    override fun onItemClick(item: String?, position: Int) {
        viewModel.sharedPreferences.edit().putString(
            Constants.Coords.LAT,
            item?.split("@@")?.get(0)
        ).apply()
        viewModel.sharedPreferences.edit()
            .putString(Constants.Coords.LON, item?.split("@@")?.get(1)).apply()
        viewModel.sharedPreferences.edit().putString(Constants.ADDRESS, item?.split("@@")?.get(2)).apply()
        init()
    }

    private fun saveCoordsToSharedPref(lat: String, lng: String, address: String) {
        binding.viewModel?.sharedPreferences?.edit()!!.putString(Constants.ADDRESS, address).apply()
        var latlang = lat.toString() + "@@" + lng.toString() + "@@" + address
        val setvalues = binding.viewModel?.sharedPreferences?.getStringSet(
            Constants.LATLANG,
            HashSet<String>()
        )
        setvalues?.add(latlang)
        binding.viewModel?.sharedPreferences?.edit()!!.putStringSet(Constants.LATLANG, setvalues)
            .apply()
        val listvalues = ArrayList<String>()
        setvalues?.let { listvalues.addAll(it) }
        dashboardCommunityListAdapter?.setData(listvalues);


    }

    private fun setAdapter() {
        val setvalues = binding.viewModel?.sharedPreferences?.getStringSet(
            Constants.LATLANG,
            HashSet<String>()
        )
        binding.viewModel?.sharedPreferences?.edit()!!.putStringSet(Constants.LATLANG, setvalues)
            .apply()
        val listvalues = ArrayList<String>()
        setvalues?.let { listvalues.addAll(it) }
        dashboardCommunityListAdapter?.setData(listvalues);
    }
}

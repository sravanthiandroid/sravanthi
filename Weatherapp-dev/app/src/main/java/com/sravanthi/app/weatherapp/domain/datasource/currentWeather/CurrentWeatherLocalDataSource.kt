package com.sravanthi.app.weatherapp.domain.datasource.currentWeather

import com.sravanthi.app.weatherapp.db.dao.CurrentWeatherDao
import com.sravanthi.app.weatherapp.db.entity.CurrentWeatherEntity
import com.sravanthi.app.weatherapp.domain.model.CurrentWeatherResponse
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class CurrentWeatherLocalDataSource @Inject constructor(
    private val currentWeatherDao: CurrentWeatherDao
) {

    fun getCurrentWeather() = currentWeatherDao.getCurrentWeather()

    fun insertCurrentWeather(currentWeather: CurrentWeatherResponse) = currentWeatherDao.deleteAndInsert(
        CurrentWeatherEntity(currentWeather)
    )
}

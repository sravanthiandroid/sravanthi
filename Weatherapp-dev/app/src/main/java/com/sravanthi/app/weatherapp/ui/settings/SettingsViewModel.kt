package com.sravanthi.app.weatherapp.ui.settings

import android.content.SharedPreferences
import com.sravanthi.app.weatherapp.core.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by sravanthi madhapuram on 07-08-2021
 */

@HiltViewModel
class SettingsViewModel @Inject internal constructor(
    val pref: SharedPreferences
) : BaseViewModel() {
    private val address: String = ""
}

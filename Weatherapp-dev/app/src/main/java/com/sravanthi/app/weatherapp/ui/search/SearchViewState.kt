package com.sravanthi.app.weatherapp.ui.search

import com.sravanthi.app.weatherapp.core.BaseViewState
import com.sravanthi.app.weatherapp.db.entity.CitiesForSearchEntity
import com.sravanthi.app.weatherapp.utils.domain.Status

/**
 * Created by sravanthi madhapuram on 07-08-2021
 */

class SearchViewState(
    val status: Status,
    val error: String? = null,
    val data: List<CitiesForSearchEntity>? = null
) : BaseViewState(status, error) {
    fun getSearchResult() = data
}

package com.sravanthi.app.weatherapp.utils

import android.content.SharedPreferences
import com.sravanthi.app.weatherapp.core.Constants

public class Utils {


    public open fun saveCoordsToSharedPref(
        lat: Double?,
        lang: Double?,
        address: String,
        sharedPreferences: SharedPreferences
    ) {
        var latlang = lat.toString() + "@@" + lang.toString() + "@@" + address
        val setvalues = sharedPreferences.getStringSet(Constants.Coords.LON, HashSet<String>())
        setvalues?.add(latlang)
        sharedPreferences.edit().putStringSet(Constants.Coords.LON, setvalues).apply()

    }

    fun deleteCoordsToSharedPref(
        lat: Double?,
        lang: Double?,
        address: String,
        sharedPreferences: SharedPreferences
    ) {
        var latlang = lat.toString() + "@@" + lang.toString() + "@@" + address
        val setvalues = sharedPreferences.getStringSet(Constants.Coords.LON, setOf(String()))
        setvalues?.add(latlang)
        sharedPreferences.edit().putStringSet(Constants.Coords.LON, setvalues).apply()

    }

    fun getFirstCity(sharedPreferences: SharedPreferences): String {
        val setvalues = ArrayList<String>()

        sharedPreferences.getStringSet(Constants.Coords.LON, HashSet<String>())?.let {
            setvalues.addAll(
                it
            )
        }
        return setvalues.get(0);
    }

    fun getFirstAllCity(sharedPreferences: SharedPreferences): ArrayList<String> {
        val setvalues = ArrayList<String>()

        sharedPreferences.getStringSet(Constants.Coords.LON, HashSet<String>())?.let {
            setvalues.addAll(
                it
            )
        }
        return setvalues;
    }
}
package com.sravanthi.app.weatherapp.ui.dashboard

import com.sravanthi.app.weatherapp.core.BaseViewState
import com.sravanthi.app.weatherapp.db.entity.ForecastEntity
import com.sravanthi.app.weatherapp.utils.domain.Status

/**
 * Created by sravanthi madhapuram on 06-08-2021
 */

class ForecastViewState(
    val status: Status,
    val error: String? = null,
    val data: ForecastEntity? = null
) : BaseViewState(status, error) {
    fun getForecast() = data
}

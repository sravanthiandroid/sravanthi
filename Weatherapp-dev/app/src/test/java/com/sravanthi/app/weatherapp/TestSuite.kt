package com.sravanthi.app.weatherapp

import android.os.Build
import com.sravanthi.app.weatherapp.dao.CitiesForSearchDaoTest
import com.sravanthi.app.weatherapp.dao.CurrentWeatherDaoTest
import com.sravanthi.app.weatherapp.dao.ForecastDaoTest
import com.sravanthi.app.weatherapp.repo.CurrentWeatherRepositoryTest
import com.sravanthi.app.weatherapp.repo.ForecastRepositoryTest
import com.sravanthi.app.weatherapp.viewModel.DashboardViewModelTest
import com.sravanthi.app.weatherapp.viewModel.SearchViewModelTest
import com.sravanthi.app.weatherapp.viewModel.WeatherDetailViewModelTest
import com.sravanthi.app.weatherapp.viewState.CurrentWeatherViewStateTest
import com.sravanthi.app.weatherapp.viewState.ForecastViewStateTest
import com.sravanthi.app.weatherapp.viewState.SearchViewStateTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config

/**
 * Created by sravanthi madhapuram on 08-08-2021
 */

@Config(sdk = [Build.VERSION_CODES.R])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    CitiesForSearchDaoTest::class,
    CurrentWeatherDaoTest::class,
    CurrentWeatherViewStateTest::class,
    DashboardViewModelTest::class,
    ForecastDaoTest::class,
    ForecastViewStateTest::class,
    SearchViewStateTest::class,
    SearchViewModelTest::class,
    WeatherDetailViewModelTest::class,
    ForecastRepositoryTest::class,
    CurrentWeatherRepositoryTest::class
)
class TestSuite
